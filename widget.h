#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QNetworkAccessManager>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_go_clicked();
    void done(QNetworkReply * reply);

private:
    Ui::Widget *ui;
    QNetworkAccessManager networkManager;

    void query(QUrl url);
};

#endif // WIDGET_H
