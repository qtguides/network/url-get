#include "widget.h"
#include "ui_widget.h"

#include <QNetworkReply>
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    connect(ui->close, SIGNAL(clicked()), QApplication::instance(), SLOT(quit()));
    connect (&networkManager, SIGNAL(finished(QNetworkReply *)), this, SLOT(done(QNetworkReply *)));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_go_clicked()
{
    QUrl url(ui->url->text());
    query(url);
}

void Widget::done(QNetworkReply * reply)
{
    QString rta;
    QVariant redirect = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
    QString redirectText = redirect.toString();

    if (reply->error() == QNetworkReply::NoError)
    {
        rta = QString(reply->readAll ());
    }
    else
    {
       rta = QString(reply->errorString ());
    }

    qDebug() << "Redirect: " + redirectText;
    this->ui->text->setPlainText(rta);
    this->ui->html->setHtml(rta);

    reply->deleteLater();

    if(!redirect.isNull())
    {
        ui->url->setText(redirectText);
        query(QUrl(redirectText));
    }
}

void Widget::query(QUrl url)
{
    QNetworkRequest request;

    request.setUrl(url);
    //request.setRawHeader("User-Agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)");
    request.setRawHeader("Content-Type", "text/html; charset=utf-8");
    request.setRawHeader("Accept-Language", "es-ar");
    request.setRawHeader("Referer", "http://www.google.com");

    qDebug() << "Url: " << url.url();
    networkManager.get(request);
}
